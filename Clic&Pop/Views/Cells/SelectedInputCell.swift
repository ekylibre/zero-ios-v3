//
//  SelectedInputCell.swift
//  Clic&Farm-iOS
//
//  Created by Jonathan DE HAAY on 22/08/2018.
//  Copyright © 2018 Ekylibre. All rights reserved.
//

import UIKit
import CoreData

protocol SelectedInputCellDelegate: class {
  func removeInputCell(_ indexPath: IndexPath)
}

class SelectedInputCell: UITableViewCell, UITextFieldDelegate {

  // MARK: - Properties

  weak var cellDelegate: SelectedInputCellDelegate?
  var addInterventionViewController: AddInterventionViewController?
  var indexPath: IndexPath!
  var type = ""

  lazy var inputInfoButton: UIButton = {
    let inputInfoButton = UIButton(frame: CGRect.zero)
    inputInfoButton.translatesAutoresizingMaskIntoConstraints = false
    return inputInfoButton
  }()

  lazy var nameLabel: UILabel = {
    let nameLabel = UILabel(frame: CGRect.zero)
    nameLabel.font = UIFont.systemFont(ofSize: 14)
    nameLabel.translatesAutoresizingMaskIntoConstraints = false
    return nameLabel
  }()

  lazy var infoLabel: UILabel = {
    let infoLabel = UILabel(frame: CGRect.zero)
    infoLabel.font = UIFont.boldSystemFont(ofSize: 14)
    infoLabel.translatesAutoresizingMaskIntoConstraints = false
    return infoLabel
  }()

  lazy var removeButton: UIButton = {
    let removeButton = UIButton(frame: CGRect.zero)
    let tintedImage = UIImage(named: "trash")?.withRenderingMode(.alwaysTemplate)
    removeButton.setImage(tintedImage, for: .normal)
    removeButton.tintColor = AppColor.AppleColors.Red
    removeButton.imageEdgeInsets = UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10)
    removeButton.addTarget(self, action: #selector(removeCell), for: .touchUpInside)
    removeButton.translatesAutoresizingMaskIntoConstraints = false
    return removeButton
  }()

  lazy var quantityLabel: UILabel = {
    let quantityLabel = UILabel(frame: CGRect.zero)
    quantityLabel.text = "quantity".localized
    quantityLabel.textColor = AppColor.TextColors.DarkGray
    quantityLabel.font = UIFont.systemFont(ofSize: 14)
    quantityLabel.translatesAutoresizingMaskIntoConstraints = false
    return quantityLabel
  }()
    
    lazy var usageLabel: UILabel = {
      let usageLabel = UILabel(frame: CGRect.zero)
      usageLabel.text = "Usage:"//"usage".localized
      usageLabel.textColor = AppColor.TextColors.DarkGray
      usageLabel.font = UIFont.systemFont(ofSize: 14)
      usageLabel.translatesAutoresizingMaskIntoConstraints = false
      return usageLabel
    }()

  lazy var quantityTextField: UITextField = {
    let quantityTextField = UITextField(frame: CGRect.zero)
    quantityTextField.placeholder = "0"
    quantityTextField.keyboardType = .decimalPad
    quantityTextField.textAlignment = .center
    quantityTextField.backgroundColor = AppColor.ThemeColors.White
    quantityTextField.layer.borderWidth = 0.5
    quantityTextField.layer.borderColor = UIColor.lightGray.cgColor
    quantityTextField.layer.cornerRadius = 5
    quantityTextField.delegate = self
    quantityTextField.addTarget(self, action: #selector(saveQuantity), for: .editingChanged)
    quantityTextField.translatesAutoresizingMaskIntoConstraints = false
    quantityTextField.accessibilityIdentifier = "item_quantity_edit"
    return quantityTextField
  }()
    
    lazy var usagePickerButton: UIButton = {
        let usagePickerButton = UIButton(frame: CGRect.zero)
        usagePickerButton.setTitle("usage", for: .normal)
        usagePickerButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
        usagePickerButton.setTitleColor(AppColor.TextColors.Black, for: .normal)
        usagePickerButton.backgroundColor = AppColor.ThemeColors.White
        usagePickerButton.layer.borderWidth = 0.5
        usagePickerButton.layer.borderColor = UIColor.lightGray.cgColor
        usagePickerButton.layer.cornerRadius = 5
        usagePickerButton.addTarget(self, action: #selector(showUsagesSpinner), for: .touchUpInside)
        usagePickerButton.translatesAutoresizingMaskIntoConstraints = false
        return usagePickerButton
    }()

  lazy var unitButton: UIButton = {
    let unitButton = UIButton(frame: CGRect.zero)
    unitButton.setTitle("unit", for: .normal)
    unitButton.titleLabel?.font = UIFont.systemFont(ofSize: 13)
    unitButton.setTitleColor(AppColor.TextColors.Black, for: .normal)
    unitButton.backgroundColor = AppColor.ThemeColors.White
    unitButton.layer.borderWidth = 0.5
    unitButton.layer.borderColor = UIColor.lightGray.cgColor
    unitButton.layer.cornerRadius = 5
    unitButton.addTarget(self, action: #selector(showUnitMeasure), for: .touchUpInside)
    unitButton.translatesAutoresizingMaskIntoConstraints = false
    return unitButton
  }()

  lazy var surfaceQuantity: UILabel = {
    let surfaceQuantity = UILabel(frame: CGRect.zero)
    surfaceQuantity.isHidden = true
    surfaceQuantity.font = UIFont.systemFont(ofSize: 15)
    surfaceQuantity.textColor = AppColor.TextColors.DarkGray
    surfaceQuantity.translatesAutoresizingMaskIntoConstraints = false
    return surfaceQuantity
  }()

  lazy var warningImageView: UIImageView = {
    let warningImageView = UIImageView(frame: CGRect.zero)
    warningImageView.isHidden = true
    warningImageView.image = UIImage(named: "warning")
    warningImageView.tintColor = .red
    warningImageView.translatesAutoresizingMaskIntoConstraints = false
    return warningImageView
  }()

  lazy var warningLabel: UILabel = {
    let warningLabel = UILabel(frame: CGRect.zero)
    warningLabel.isHidden = false
    warningLabel.font = UIFont.systemFont(ofSize: 13)
    warningLabel.textColor = .red
    warningLabel.text = "invalid_dose".localized
    warningLabel.translatesAutoresizingMaskIntoConstraints = false
    return warningLabel
  }()

  lazy var surfaceQuantityTopConstraint: NSLayoutConstraint = {
    let surfaceQuantityTopConstraint = NSLayoutConstraint(
      item: surfaceQuantity,
      attribute: .top,
      relatedBy: .equal,
      toItem: quantityTextField,
      attribute: .bottom,
      multiplier: 1,
      constant: 5)

    surfaceQuantityTopConstraint.isActive = true
    return surfaceQuantityTopConstraint
  }()

  var inputUnit: String!
  var selectedPhytoUsages = [PhytoUsage]()

  // MARK: - Initialization

  override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
    super.init(style: style, reuseIdentifier: reuseIdentifier)
    setupCell()
  }

  private func setupCell() {
    backgroundColor = AppColor.CellColors.LightGray
    selectionStyle = .none
    contentView.addSubview(inputInfoButton)
    contentView.addSubview(nameLabel)
    contentView.addSubview(infoLabel)
    contentView.addSubview(removeButton)
    contentView.addSubview(quantityLabel)
    contentView.addSubview(usageLabel)
    contentView.addSubview(quantityTextField)
    contentView.addSubview(unitButton)
    contentView.addSubview(usagePickerButton)
    contentView.addSubview(warningImageView)
    contentView.addSubview(warningLabel)
    contentView.addSubview(surfaceQuantity)
    contentView.addConstraint(surfaceQuantityTopConstraint)
    setupLayout()
  }

  private func setupLayout() {
    NSLayoutConstraint.activate([
      inputInfoButton.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
      inputInfoButton.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
      inputInfoButton.widthAnchor.constraint(equalToConstant: 24),
      inputInfoButton.heightAnchor.constraint(equalToConstant: 24),
      
      nameLabel.leadingAnchor.constraint(equalTo: inputInfoButton.trailingAnchor, constant: 10),
      nameLabel.centerYAnchor.constraint(equalTo: inputInfoButton.centerYAnchor),
      
      infoLabel.leadingAnchor.constraint(equalTo: nameLabel.trailingAnchor, constant: 5),
      infoLabel.trailingAnchor.constraint(lessThanOrEqualTo: removeButton.leadingAnchor, constant: -10),
      infoLabel.centerYAnchor.constraint(equalTo: inputInfoButton.centerYAnchor),
      
      removeButton.trailingAnchor.constraint(equalTo: contentView.trailingAnchor),
      removeButton.topAnchor.constraint(equalTo: contentView.topAnchor),
      removeButton.widthAnchor.constraint(equalToConstant: 40),
      removeButton.heightAnchor.constraint(equalToConstant: 40),
      
      usageLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
      usageLabel.topAnchor.constraint(equalTo: inputInfoButton.bottomAnchor, constant: 15),
     
      quantityLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 10),
      quantityLabel.topAnchor.constraint(equalTo: usageLabel.bottomAnchor, constant: 15),

      quantityTextField.leadingAnchor.constraint(equalTo: quantityLabel.trailingAnchor, constant: 15),
      quantityTextField.centerYAnchor.constraint(equalTo: quantityLabel.centerYAnchor),
      quantityTextField.widthAnchor.constraint(equalToConstant: 70),
      quantityTextField.heightAnchor.constraint(equalToConstant: 30),

      unitButton.leadingAnchor.constraint(equalTo: quantityTextField.trailingAnchor, constant: 10),
      unitButton.centerYAnchor.constraint(equalTo: quantityTextField.centerYAnchor),
      
      usagePickerButton.leadingAnchor.constraint(equalTo: usageLabel.trailingAnchor, constant: 15),
      usagePickerButton.centerYAnchor.constraint(equalTo: usageLabel.centerYAnchor),
      usagePickerButton.widthAnchor.constraint(equalToConstant: 200),
      usagePickerButton.heightAnchor.constraint(equalToConstant: 30),

      warningImageView.leadingAnchor.constraint(equalTo: quantityTextField.leadingAnchor),
      warningImageView.topAnchor.constraint(equalTo: quantityTextField.bottomAnchor, constant: 5),
      warningImageView.widthAnchor.constraint(equalToConstant: 10),
      warningImageView.heightAnchor.constraint(equalToConstant: 10),
      warningLabel.leadingAnchor.constraint(equalTo: warningImageView.trailingAnchor, constant: 3),
      warningLabel.centerYAnchor.constraint(equalTo: warningImageView.centerYAnchor),
      surfaceQuantity.leadingAnchor.constraint(equalTo: quantityTextField.leadingAnchor)
      ])
  }

  required init?(coder aDecoder: NSCoder) {
    fatalError("init(coder:) has not been implemented")
  }

  // MARK: - Text field

  func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange,
                 replacementString string: String) -> Bool {
    guard let oldText = textField.text, let r = Range(range, in: oldText) else {
      return true
    }

    let newText = oldText.replacingCharacters(in: r, with: string)
    var numberOfDecimalDigits = 0

    if newText.components(separatedBy: ".").count > 2 || newText.components(separatedBy: ",").count > 2 {
      return false
    } else if let dotIndex = (newText.contains(",") ? newText.index(of: ",") : newText.index(of: ".")) {
      numberOfDecimalDigits = newText.distance(from: dotIndex, to: newText.endIndex) - 1
    }
    return numberOfDecimalDigits <= 2 && newText.count <= 16
  }

  // MARK: - Actions

  private func checkIfUnitContains(unit: String, generalUnits: [String]) -> Bool {
    var contains = false

    for generalUnit in generalUnits {
      if unit.contains(generalUnit) {
        contains = true
      }
    }
    return contains
  }

  private func defineInputsPerSurface(units: [String]) -> [String] {
    var unitsPerSurface = [String]()

    for unit in units {
      let unitPerHectare = unit + "_PER_HECTARE"
      let unitPerSquareMeter = unit + "_PER_SQUARE_METER"

      unitsPerSurface.append(unit)
      unitsPerSurface.append(unitPerHectare)
      unitsPerSurface.append(unitPerSquareMeter)
    }
    return unitsPerSurface
  }

  private func defineInputsUnitsBasedOnCreatedUnit(unit: String) -> [String]? {
    let generalMassUnits = ["GRAM", "KILOGRAM", "QUINTAL", "TON"]
    let generalCountUnits = ["UNITY", "THOUSAND"]
    let generalVolumeUnits = ["LITER", "HECTOLITER", "CUBIC_METER"]

    if checkIfUnitContains(unit: unit, generalUnits: generalMassUnits) {
      return defineInputsPerSurface(units: generalMassUnits)
    } else if checkIfUnitContains(unit: unit, generalUnits: generalCountUnits) {
      return defineInputsPerSurface(units: generalCountUnits)
    } else {
      if checkIfUnitContains(unit: unit, generalUnits: generalVolumeUnits) {
        return defineInputsPerSurface(units: generalVolumeUnits)
      } else {
        return [unit]
      }
    }
  }

  @objc private func showUnitMeasure(sender: UIButton) {
    guard let selectedUnit = sender.titleLabel?.text else { return }
    let units = defineInputsUnitsBasedOnCreatedUnit(unit: inputUnit)

    if units != nil && units!.count > 1 {
      addInterventionViewController?.customPickerView.values = units!
      addInterventionViewController?.customPickerView.pickerView.reloadComponent(0)
      addInterventionViewController?.customPickerView.selectLastValue(selectedUnit)
      addInterventionViewController?.customPickerView.closure = { (value) in
        self.addInterventionViewController?.selectedInputs[self.indexPath.row].setValue(value, forKey: "unit")
        self.addInterventionViewController?.selectedInputsTableView.reloadData()
      }
      addInterventionViewController?.customPickerView.isHidden = false
    }
  }
    
    @objc private func showUsagesSpinner(sender: UIButton) {
      guard let selectedUsage = sender.titleLabel?.text else { return }
        let usages = selectedPhytoUsages.map { $0.ephy_usage_phrase! }

      if usages.count > 1 {
        addInterventionViewController?.usagesPickerView.values = usages
        addInterventionViewController?.usagesPickerView.pickerView.reloadComponent(0)
        addInterventionViewController?.usagesPickerView.selectLastValue(selectedUsage)
        addInterventionViewController?.usagesPickerView.closure = { (value) in
            for phytoUsage in self.selectedPhytoUsages {
                if phytoUsage.ephy_usage_phrase == value {
                    let test = phytoUsage.id
                    self.addInterventionViewController?.selectedInputs[self.indexPath.row].setValue(phytoUsage.id, forKey: "usageId")
                    self.displayWarningIfInvalidDoses(self.addInterventionViewController?.selectedInputs[self.indexPath.row] as! InterventionPhytosanitary)
                }
            }
          self.addInterventionViewController?.selectedInputsTableView.reloadData()
        }
        addInterventionViewController?.usagesPickerView.isHidden = false
      }
    }

  @objc private func removeCell() {
    cellDelegate?.removeInputCell(indexPath)
  }

  private func checkPhytosanitaryDoses(_ phytoID: Int32, _ quantity: Float) -> Bool {
    if let asset = NSDataAsset(name: "phytosanitary-doses") {
      do {
        let jsonResult = try JSONSerialization.jsonObject(with: asset.data)
        let phytosanitaryDoses = jsonResult as? [[String: Any]]

        for phytosanitaryDose in phytosanitaryDoses! {
          let productID = phytosanitaryDose["product_id"] as! NSNumber

          if Int(truncating: productID) == phytoID {
            let dose = phytosanitaryDose["dose"] as! NSNumber

            return quantity <= Float(truncating: dose)
          }
        }
      } catch let error as NSError {
        print("Lexicon fetch failed. \(error)")
      }
    } else {
      print("phytosanitary-doses.json not found")
    }
    return true
  }

  func displayWarningIfInvalidDoses(_ interventionPhyto: InterventionPhytosanitary) {
   
    if let phytoUsageId = interventionPhyto.usageId {
        
        let appDelegate = UIApplication.shared.delegate as? AppDelegate
        let managedContext = appDelegate!.persistentContainer.viewContext
        let entitiesFetchRequest = NSFetchRequest<NSManagedObject>(entityName: "PhytoUsage")
        entitiesFetchRequest.predicate = NSPredicate(format: "id = %@", phytoUsageId)
        
        do {
          let entities = try managedContext.fetch(entitiesFetchRequest)
          let phytoUsage = entities[0] as! PhytoUsage
            
            if (phytoUsage.dose_quantity < interventionPhyto.quantity) {
                warningLabel.isHidden = false
                warningImageView.isHidden = false
                surfaceQuantityTopConstraint.constant = 20
            } else {
                warningLabel.isHidden = true
                warningImageView.isHidden = true
                surfaceQuantityTopConstraint.constant = 5
            }
        } catch let error as NSError {
          print("Could not save or fetch. \(error), \(error.userInfo)")
        }
    } else {
      warningLabel.isHidden = true
      warningImageView.isHidden = true
      surfaceQuantityTopConstraint.constant = 5
    }
  }

  @objc private func saveQuantity() {
    addInterventionViewController?.selectedInputs[indexPath.row].setValue(
      quantityTextField.text!.floatValue, forKey: "quantity")
    addInterventionViewController?.updateQuantityLabel(indexPath: indexPath)
    
    if addInterventionViewController?.selectedInputs[indexPath.row] is InterventionPhytosanitary {
      displayWarningIfInvalidDoses(addInterventionViewController?.selectedInputs[indexPath.row] as! InterventionPhytosanitary)
    }
  }
}
