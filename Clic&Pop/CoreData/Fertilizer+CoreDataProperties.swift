//
//  Fertilizer+CoreDataProperties.swift
//  Clic&Pop
//
//  Created by Manon Bossuet on 05/03/2020.
//  Copyright © 2020 Ekylibre. All rights reserved.
//
//

import Foundation
import CoreData


extension Fertilizer {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<Fertilizer> {
        return NSFetchRequest<Fertilizer>(entityName: "Fertilizer")
    }

    @NSManaged public var derivativeOf: String?
    @NSManaged public var ekyID: Int32
    @NSManaged public var name: String?
    @NSManaged public var nature: String?
    @NSManaged public var nitrogenConcentration: NSNumber?
    @NSManaged public var phosphorusConcentration: NSNumber?
    @NSManaged public var potassiumConcentration: NSNumber?
    @NSManaged public var referenceID: Int32
    @NSManaged public var registered: Bool
    @NSManaged public var sulfurTrioxydeConcentration: NSNumber?
    @NSManaged public var unit: String?
    @NSManaged public var used: Bool
    @NSManaged public var interventionFertilizers: NSSet?

}

// MARK: Generated accessors for interventionFertilizers
extension Fertilizer {

    @objc(addInterventionFertilizersObject:)
    @NSManaged public func addToInterventionFertilizers(_ value: InterventionFertilizer)

    @objc(removeInterventionFertilizersObject:)
    @NSManaged public func removeFromInterventionFertilizers(_ value: InterventionFertilizer)

    @objc(addInterventionFertilizers:)
    @NSManaged public func addToInterventionFertilizers(_ values: NSSet)

    @objc(removeInterventionFertilizers:)
    @NSManaged public func removeFromInterventionFertilizers(_ values: NSSet)

}
