//
//  DefineInterventionsDisplayMode.swift
//  Clic&Farm-iOS
//
//  Created by Jonathan DE HAAY on 08/10/2018.
//  Copyright © 2018 Ekylibre. All rights reserved.
//

import UIKit
import CoreData

extension AddInterventionViewController {

  private func disableUserInteraction() {
    if interventionState == InterventionState.Validated.rawValue {
      loadsTableView.isUserInteractionEnabled = false
      harvestNatureButton.isUserInteractionEnabled = false
      cropsView.tableView.isUserInteractionEnabled = false
      workingPeriodTapGesture.isEnabled = false
      selectedInputsTableView.isUserInteractionEnabled = false
      irrigationTapGesture.isEnabled = false
      selectedEquipmentsTableView.isUserInteractionEnabled = false
      selectedMaterialsTableView.isUserInteractionEnabled = false
    }
  }

  private func loadWorkingPeriod() {
    let dateFormatter = DateFormatter()
    let selectedDate: String
    let workingPeriods = (currentIntervention?.workingPeriods?.allObjects.first as? WorkingPeriod)
    let date = workingPeriods?.executionDate
    let duration = workingPeriods?.hourDuration

    dateFormatter.locale = Locale(identifier: "locale".localized)
    dateFormatter.dateFormat = "d MMM"
    if date != nil && duration != nil {
      selectedDate = dateFormatter.string(from: date!)
      selectedWorkingPeriodLabel.text = String(format: "%@ • %g h", selectedDate, duration!)
      workingPeriodDateButton.setTitle(selectedDate, for: .normal)
      selectDateView.datePicker.date = date!
    }
    if interventionState == InterventionState.Validated.rawValue {
      workingPeriodExpandImageView.isHidden = true
    }
  }

  private func loadIrrigation() {
    if interventionType == .Irrigation {
      let waterQuantity = currentIntervention?.waterQuantity

      irrigationVolumeTextField.text = (waterQuantity as NSNumber?)?.stringValue
      irrigationUnitButton.setTitle(currentIntervention?.waterUnit?.localized, for: .normal)
      updateIrrigation()
      if interventionState == InterventionState.Validated.rawValue {
        irrigationExpandImageView.isHidden = true
      } else {
        irrigationExpandImageView.transform = irrigationExpandImageView.transform.rotated(by: CGFloat.pi)
      }
      tapIrrigationView(self)
    }
  }

  private func loadInputs() {
    let interventionSeeds = currentIntervention?.interventionSeeds
    let interventionPhytosanitaries = currentIntervention?.interventionPhytosanitaries
    let interventionFertilizers = currentIntervention?.interventionFertilizers

    for case let interventionSeed as InterventionSeed in interventionSeeds! {
      let seed = interventionSeed.seed
      let quantity = interventionSeed.quantity
      let unit = interventionSeed.unit

      if seed != nil {
        selectInput(seed!, quantity: quantity, unit: unit, usageId: nil, calledFromCreatedIntervention: true)
      }
    }
    for case let interventionPhyto as InterventionPhytosanitary in interventionPhytosanitaries! {
      let phyto = interventionPhyto.phyto
      let quantity = interventionPhyto.quantity
      let unit = interventionPhyto.unit

      if phyto != nil {
        selectInput(phyto!, quantity: quantity, unit: unit, usageId: interventionPhyto.usageId, calledFromCreatedIntervention: true)
      }
    }
    for case let interventionFertilizer as InterventionFertilizer in interventionFertilizers! {
      let fertilizer = interventionFertilizer.fertilizer
      let quantity = interventionFertilizer.quantity
      let unit = interventionFertilizer.unit

      if fertilizer != nil {
        selectInput(fertilizer!, quantity: quantity, unit: unit, usageId: nil, calledFromCreatedIntervention: true)
      }
    }
    refreshSelectedInputs()
  }

  private func loadMaterials() {
    if let interventionMaterials = currentIntervention?.interventionMaterials {
      for case let interventionMaterial as InterventionMaterial in interventionMaterials {
        let material = interventionMaterial.material
        let quantity = interventionMaterial.quantity
        let unit = interventionMaterial.unit

        if material != nil {
          selectMaterial(material!, quantity: quantity, unit: unit!, calledFromCreatedIntervention: true)
        }
      }
    }
    updateSelectedMaterialsView(calledFromCreatedIntervention: true)
  }

  private func loadHarvest() {
    if let interventionHarvests = currentIntervention?.harvests {
      for case let harvest as Harvest in interventionHarvests {
        let storage = harvest.storage
        let type = harvest.type
        let number = harvest.number
        let unit = harvest.unit
        let quantity = harvest.quantity

        createHarvest(storage, type, number, unit, quantity)
      }
    }
    loadsAddButton.isHidden = (interventionState == InterventionState.Validated.rawValue)
    if selectedHarvests.count > 0 {
      harvestNatureButton.setTitle(selectedHarvests.first?.type?.localized, for: .normal)
      tapHarvestView()
      tapHarvestView()
    }
  }

  private func loadEquipments() {
    if let interventionEquipments = currentIntervention?.interventionEquipments {
      for case let interventionEquipment as InterventionEquipment in interventionEquipments {
        let equipment = interventionEquipment.equipment

        if equipment != nil {
          selectEquipment(equipment!, calledFromCreatedIntervention: true)
        }
      }
    }
    updateSelectedEquipmentsView(calledFromCreatedIntervention: true)
  }

  private func loadInterventionInEditableMode() {
    interventionType = InterventionType(rawValue: currentIntervention.type!)
    loadWorkingPeriod()
    loadInputs()
    loadMaterials()
    loadIrrigation()
    loadEquipments()
    loadHarvest()
    dimView.isHidden = false
  }

  func loadInterventionInAppropriateMode() {
    if interventionState == InterventionState.Created.rawValue ||
      interventionState == InterventionState.Synced.rawValue ||
      interventionState == InterventionState.Validated.rawValue  {
      loadInterventionInEditableMode()
    }
  }
}
