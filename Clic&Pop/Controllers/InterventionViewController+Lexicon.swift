//
//  InterventionViewController+Lexicon.swift
//  Clic&Farm-iOS
//
//  Created by Guillaume Roux on 08/10/2018.
//  Copyright © 2018 Ekylibre. All rights reserved.
//

import UIKit
import CoreData

extension InterventionViewController {

  func loadRegisteredInputs() {
    guard let assets = openAssets() else { return }
    let decoder = JSONDecoder()

    do {
        let registeredSeeds = try decoder.decode([RegisteredSeed].self, from: assets[0].data)
        let registeredPhytos = try decoder.decode([RegisteredPhyto].self, from: assets[1].data)
        let registeredFertilizers = try decoder.decode([RegisteredFertilizer].self, from: assets[2].data)
        let registeredTrichogrammas = try decoder.decode([RegisteredTrichogramma].self, from: assets[3].data)
        let registeredUsages = try decoder.decode([RegisteredUsage].self, from: assets[4].data)

        saveSeeds(registeredSeeds)
        savePhytos(registeredPhytos)
        saveFertilizers(registeredFertilizers)
        saveTrichogrammas(registeredTrichogrammas)
        saveUsages(registeredUsages)
    } catch let jsonError {
      print(jsonError)
    }
  }

  private func openAssets() -> [NSDataAsset]? {
    var assets = [NSDataAsset]()
    let assetNames = ["seeds", "phytosanitary-products", "fertilizers", "trichogrammas", "phytosanitary-usages"]

    for assetName in assetNames {
      if let asset = NSDataAsset(name: assetName) {
        assets.append(asset)
      } else {
        print(assetName + ": asset not found")
        return nil
      }
    }
    return assets
  }

  private func saveSeeds(_ registeredSeeds: [RegisteredSeed]) {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }

    let managedContext = appDelegate.persistentContainer.viewContext

    for registeredSeed in registeredSeeds {
      let seed = Seed(context: managedContext)

      seed.registered = true
      seed.ekyID = 0
      seed.referenceID = Int32(registeredSeed.id)
      seed.specie = registeredSeed.specie.uppercased()
      seed.variety = registeredSeed.variety
      seed.unit = "SEED_PER_HECTARE"
      seed.used = false
    }

    do {
      try managedContext.save()
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
  }
    
    private func saveTrichogrammas(_ registeredTrichogrammas: [RegisteredTrichogramma]) {
      guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
      }

      let managedContext = appDelegate.persistentContainer.viewContext

      for registeredTrichogramma in registeredTrichogrammas {
        let tricho = Trichogramma(context: managedContext)

        tricho.registered = true
        tricho.ekyID = 0
        tricho.referenceID = Int32(registeredTrichogramma.id)
        tricho.name = registeredTrichogramma.name.uppercased()
        tricho.unit = "UNITY"
        tricho.used = false
      }

      do {
        try managedContext.save()
      } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
      }
    }
    
    private func saveUsages(_ registeredUsages: [RegisteredUsage]) {
      guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
        return
      }

      let managedContext = appDelegate.persistentContainer.viewContext

      for registeredUsage in registeredUsages {
        let usage = PhytoUsage(context: managedContext)

        usage.id = registeredUsage.id
        usage.ephy_usage_phrase = registeredUsage.ephy_usage_phrase
        
        if registeredUsage.applications_count != nil {
            usage.applications_count = Int32(registeredUsage.applications_count!)
        } else {
            usage.applications_count = -1
        }
        
        usage.dose_quantity = registeredUsage.dose_quantity
        usage.dose_unit = registeredUsage.dose_unit.uppercased()
        usage.dose_unit_name = registeredUsage.dose_unit_name
        usage.pre_harvest_delay = Int32(registeredUsage.pre_harvest_delay)
        if registeredUsage.untreated_buffer_plants != nil {
            usage.untreated_buffer_plants = Int32(registeredUsage.untreated_buffer_plants!)
        } else {
            usage.untreated_buffer_plants = -1
        }
        
        if registeredUsage.untreated_buffer_arthropod != nil {
            usage.untreated_buffer_arthropod = Int32(registeredUsage.untreated_buffer_arthropod!)
        } else {
            usage.untreated_buffer_arthropod = -1
        }
        
        if registeredUsage.untreated_buffer_aquatic != nil {
            usage.untreated_buffer_aquatic = Int32(registeredUsage.untreated_buffer_aquatic!)
        } else {
            usage.untreated_buffer_aquatic = -1
        }
        
        usage.product_id = Int64(registeredUsage.product_id)
      }

      do {
        try managedContext.save()
      } catch let error as NSError {
        print("Could not save. \(error), \(error.userInfo)")
      }
    }

  private func savePhytos(_ registeredPhytos: [RegisteredPhyto]) {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }

    let managedContext = appDelegate.persistentContainer.viewContext

    for registeredPhyto in registeredPhytos {
      let phyto = Phyto(context: managedContext)

      phyto.registered = true
      phyto.ekyID = 0
      phyto.referenceID = Int32(registeredPhyto.id)
      phyto.name = registeredPhyto.name
      phyto.nature = registeredPhyto.nature
      phyto.maaID = registeredPhyto.maaid
      phyto.mixCategoryCode = registeredPhyto.mixCategoryCode
      phyto.inFieldReentryDelay = Int32(registeredPhyto.inFieldReentryDelay)
      phyto.firmName = registeredPhyto.firmName
      phyto.unit = "LITER_PER_HECTARE"
      phyto.used = false
        phyto.operatorProtectionMentions = (registeredPhyto.operatorProtectionMentions ?? "")
    }

    do {
      try managedContext.save()
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
  }

  private func saveFertilizers(_ registeredFertilizers: [RegisteredFertilizer]) {
    guard let appDelegate = UIApplication.shared.delegate as? AppDelegate else {
      return
    }

    let managedContext = appDelegate.persistentContainer.viewContext

    for registeredFertilizer in registeredFertilizers {
      let fertilizer = Fertilizer(context: managedContext)

      fertilizer.registered = true
      fertilizer.ekyID = 0
      fertilizer.referenceID = Int32(registeredFertilizer.id)
      fertilizer.name = registeredFertilizer.name.uppercased()
      fertilizer.derivativeOf = registeredFertilizer.derivativeOf
      fertilizer.nature = registeredFertilizer.nature
        
        fertilizer.nitrogenConcentration = registeredFertilizer.nitrogenConcentration as NSNumber?
      fertilizer.phosphorusConcentration = registeredFertilizer.phosphorusConcentration as NSNumber?
      fertilizer.potassiumConcentration = registeredFertilizer.potassiumConcentration as NSNumber?
      fertilizer.sulfurTrioxydeConcentration = registeredFertilizer.sulfurTrioxydeConcentration as NSNumber?
      fertilizer.unit = registeredFertilizer.unit.uppercased()
      fertilizer.used = false
    }

    do {
      try managedContext.save()
    } catch let error as NSError {
      print("Could not save. \(error), \(error.userInfo)")
    }
  }
}
