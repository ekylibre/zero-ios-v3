//
//  DetailedInterventionViewController+WorkingPeriod.swift
//  Clic&Farm-iOS
//
//  Created by Guillaume Roux on 16/10/2018.
//  Copyright © 2018 Ekylibre. All rights reserved.
//

import UIKit

extension AddInterventionViewController {

  // MARK: - Initialization

  func setupWorkingPeriodView() {
    let currentDateString: String = {
      let dateFormatter = DateFormatter()
      dateFormatter.locale = Locale(identifier: "locale".localized)
      dateFormatter.dateFormat = "d MMM"
      return dateFormatter.string(from: Date())
    }()

    selectDateView = SelectDateView(frame: CGRect.zero)
    workingPeriodDateButton.setTitle(currentDateString, for: .normal)
    workingPeriodDateButton.layer.borderWidth = 0.5
    workingPeriodDateButton.layer.borderColor = UIColor.lightGray.cgColor
    workingPeriodDateButton.layer.cornerRadius = 5
    workingPeriodDateButton.clipsToBounds = true
    selectDateView.cancelButton.addTarget(self, action: #selector(cancelSelection), for: .touchUpInside)
    selectDateView.doneButton.addTarget(self, action: #selector(validateDate), for: .touchUpInside)
  }

  // MARK: - Actions

  @IBAction func tapWorkingPeriodView(_ sender: Any) {
    let shouldExpand = (workingPeriodHeightConstraint.constant == 70)
    let dateString = workingPeriodDateButton.titleLabel!.text!

    view.endEditing(true)
    workingPeriodHeightConstraint.constant = shouldExpand ? 155 : 70
    selectedWorkingPeriodLabel.text = String(format: "%@", dateString)
    selectedWorkingPeriodLabel.isHidden = shouldExpand
    workingPeriodDateButton.isHidden = !shouldExpand
    workingPeriodExpandImageView.transform = workingPeriodExpandImageView.transform.rotated(by: CGFloat.pi)
  }

  @IBAction private func selectDate(_ sender: Any) {
    selectDateView.show()

    UIView.animate(withDuration: 0.5, animations: {
      if #available(iOS 13.0, *) {
          let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
           statusBar.backgroundColor = AppColor.StatusBarColors.Black
           UIApplication.shared.keyWindow?.addSubview(statusBar)
      } else {
           UIApplication.shared.statusBarView?.backgroundColor = AppColor.StatusBarColors.Black
      }
    })
  }

  @objc private func cancelSelection() {
    selectDateView.close()

    UIView.animate(withDuration: 0.5, animations: {
      if #available(iOS 13.0, *) {
          let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
           statusBar.backgroundColor = AppColor.StatusBarColors.Blue
           UIApplication.shared.keyWindow?.addSubview(statusBar)
      } else {
           UIApplication.shared.statusBarView?.backgroundColor = AppColor.StatusBarColors.Blue
      }
    }, completion: { _ in
      self.resetPickerDate()
    })
  }

  private func resetPickerDate() {
    guard let title = self.workingPeriodDateButton.titleLabel?.text else { return }
    let dateFormatter: DateFormatter = {
      let dateFormatter = DateFormatter()
      dateFormatter.locale = Locale(identifier: "locale".localized)
      dateFormatter.dateFormat = "d MMM"
      return dateFormatter
    }()
    guard let titleDate = dateFormatter.date(from: title) else { return }
    var components = Calendar.current.dateComponents([.day, .month, .year], from: titleDate)
    components.year = Calendar.current.component(.year, from: Date())
    guard let selectedDate = Calendar.current.date(from: components) else { return }

    self.selectDateView.datePicker.date = selectedDate
  }

  @objc private func validateDate() {
    let selectedDate: String = {
      let dateFormatter = DateFormatter()
      dateFormatter.locale = Locale(identifier: "locale".localized)
      dateFormatter.dateFormat = "d MMM"
      return dateFormatter.string(from: selectDateView.datePicker.date)
    }()

    workingPeriodDateButton.setTitle(selectedDate, for: .normal)
    selectDateView.close()

    UIView.animate(withDuration: 0.5, animations: {
      if #available(iOS 13.0, *) {
          let statusBar = UIView(frame: UIApplication.shared.keyWindow?.windowScene?.statusBarManager?.statusBarFrame ?? CGRect.zero)
           statusBar.backgroundColor = AppColor.StatusBarColors.Blue
           UIApplication.shared.keyWindow?.addSubview(statusBar)
      } else {
           UIApplication.shared.statusBarView?.backgroundColor = AppColor.StatusBarColors.Blue
      }
    })
  }

  @objc private func updateDurationUnit() {
    let duration = workingPeriodDurationTextField.text!.floatValue

    workingPeriodUnitLabel.text = (duration <= 1) ? "hour".localized : "hours".localized
  }
}
