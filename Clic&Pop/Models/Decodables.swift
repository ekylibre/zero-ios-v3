//
//  Decodables.swift
//  Clic&Farm-iOS
//
//  Created by Guillaume Roux on 04/09/2018.
//  Copyright © 2018 Ekylibre. All rights reserved.
//

import Foundation

struct RegisteredSeed: Decodable {
  var id: Int
  var specie: String
  var variety: String

  enum CodingKeys: String, CodingKey {
    case id, specie, variety
  }
}

struct RegisteredTrichogramma: Decodable {
  var id: Int
  var name: String

  enum CodingKeys: String, CodingKey {
    case id, name
  }
}

struct RegisteredUsage: Decodable {
    var id: String
    var ephy_usage_phrase: String
    var dose_quantity: Float
    var dose_unit: String
    var dose_unit_name: String
    var applications_count: Int?
    var pre_harvest_delay: Int
    var untreated_buffer_arthropod: Int?
    var untreated_buffer_aquatic: Int?
    var untreated_buffer_plants: Int?
    var product_id: Int

    enum CodingKeys: String, CodingKey {
        case id, ephy_usage_phrase, dose_quantity, dose_unit_name, dose_unit, applications_count, pre_harvest_delay, untreated_buffer_arthropod, untreated_buffer_aquatic, untreated_buffer_plants, product_id
    }
}

struct RegisteredPhyto: Decodable {
  var id: Int
  var name: String
  var nature: String
  var maaid: String
  var mixCategoryCode: String
  var inFieldReentryDelay: Int
  var firmName: String
    var operatorProtectionMentions: String?

  enum CodingKeys: String, CodingKey {
    case id, name, nature, maaid
    case mixCategoryCode = "mix_category_code"
    case inFieldReentryDelay = "in_field_reentry_delay"
    case firmName = "firm_name"
    case operatorProtectionMentions = "operator_protection_mentions"
  }
}

struct RegisteredFertilizer: Decodable {
  var id: Int
  var name: String
  var unit: String
  var labelFra: String
  var derivativeOf: String?
  var nature: String
  var nitrogenConcentration: Float?
  var phosphorusConcentration: Float?
  var potassiumConcentration: Float?
  var sulfurTrioxydeConcentration: Float?

  enum CodingKeys: String, CodingKey {
    case id, name, nature, unit
    case labelFra = "label_fra"
    case derivativeOf = "derivative_of"
    case nitrogenConcentration = "nitrogen_concentration"
    case phosphorusConcentration = "phosphorus_concentration"
    case potassiumConcentration = "potassium_concentration"
    case sulfurTrioxydeConcentration = "sulfur_trioxyde_concentration"
  }
}
