if [ -z "$1" ]; then
    echo "ERROR: Missing API URL"
    echo "Usage: ./get_schema_json.sh <API_URL>"
    echo "Exemple: ./get_schema_json.sh https://api.clicandpop.farm"
    exit 1
fi

query_result=$(curl -H "Accept: application/json" \
    -H "Content-Type:application/json" \
    -X POST --data '{"client_id":"22cc06685d56cace7174f2e0a0ae643024892157b4a3f7931f6266d29f665715", "client_secret":"7a259f30aa42a0b8ba3ea3a0686cbaefa013cc08985139a7cdebef965e134f1a", "grant_type":"password", "username":"dev-mobile@ekylibre.com", "password":"Canard123!", "scope":"public read:profile read:lexicon read:plots read:crops read:interventions write:interventions read:equipment write:equipment read:articles write:articles read:person write:person"}' \
    $1/oauth/token)

access_token=$(python3 -c "import json; print(json.loads('$query_result')['access_token'])")

apollo schema:download --endpoint=$1/v1/graphql schema.json --header="Authorization: Bearer $access_token"

