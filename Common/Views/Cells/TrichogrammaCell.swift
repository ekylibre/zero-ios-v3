//
//  File.swift
//  Clic&Farm-iOS
//
//  Created by Manon Bossuet on 26/02/2020.
//  Copyright © 2020 Ekylibre. All rights reserved.
//

import UIKit

class TrichogrammaCell: UITableViewCell {
    
    lazy var nameLabel: UILabel = {
      let nameLabel = UILabel(frame: CGRect.zero)
      nameLabel.font = UIFont.boldSystemFont(ofSize: 14)
      nameLabel.translatesAutoresizingMaskIntoConstraints = false
      return nameLabel
    }()

    lazy var starImageView: UIImageView = {
      let starImageView = UIImageView(frame: CGRect.zero)
      starImageView.image = UIImage(named: "star")?.withRenderingMode(.alwaysTemplate)
      starImageView.tintColor = AppColor.BarColors.Green
      starImageView.isHidden = true
      starImageView.translatesAutoresizingMaskIntoConstraints = false
      return starImageView
    }()

    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
      super.init(style: style, reuseIdentifier: reuseIdentifier)
      setupCell()
    }

    private func setupCell() {
      contentView.addSubview(nameLabel)
      contentView.addSubview(starImageView)
      setupLayout()
    }

    private func setupLayout() {
      NSLayoutConstraint.activate([
        nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 10),
        nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 15),
        nameLabel.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
        
        starImageView.centerYAnchor.constraint(equalTo: contentView.centerYAnchor),
        starImageView.heightAnchor.constraint(equalToConstant: 20),
        starImageView.trailingAnchor.constraint(equalTo: contentView.trailingAnchor, constant: -15),
        starImageView.widthAnchor.constraint(equalToConstant: 20)
        ])
    }

    required init?(coder aDecoder: NSCoder) {
      fatalError("init(coder:) has not been implemented")
    }
}
